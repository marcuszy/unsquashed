// Here is some very deep and important code.

function curryfy(f) {
  return function(a) {
    return function(b) {
      return f(a,b);
    }
  }
}

function calcSum(augend, addend) {
  return augend + addend;
}

let currySum = curryfy(calcSum);

console.log(currySum(5)(6));